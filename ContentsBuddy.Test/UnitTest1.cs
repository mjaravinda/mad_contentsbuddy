using ContentsBuddy.Repositories;
using ContentsBuddy.ViewModels;

namespace ContentsBuddy.Test
{
    public class UnitTest1
    {
        [Fact]
        public async void GetAllContactsCountFact()
        {
            var expected =await new ContactRepository().GetAllContacts();
   
            var vm = new ContactVM();
            await vm.GetAllContacts();
        
            Assert.Equal(expected?.Count, vm.AllContacts.Count);
           

        }
        
    }
}