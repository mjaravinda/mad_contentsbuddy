﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ContentsBuddy.Repositories
{
    public class DbContext
    {
        protected SQLiteAsyncConnection database;
        protected async void Init()
        {
            if (database is not null) return;
            database = new SQLiteAsyncConnection(Constraint.DatabasePath, Constraint.Flags);
            var result = await database.CreateTableAsync<Model.Contact>();
        }
    }
}
