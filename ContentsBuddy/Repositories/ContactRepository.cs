﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContentsBuddy.Repositories
{
    public class ContactRepository:DbContext
    {
        SQLiteAsyncConnection db;
        public ContactRepository()
        {
            base.Init();
            db = base.database;
        }

        /// <summary>
        /// View All Contacts
        /// </summary>
        /// <returns></returns>
        public async Task<List<Model.Contact>> GetAllContacts()
        {
            var data = await db.Table<Model.Contact>().OrderBy(o => o.Name).ToListAsync();
            if (data != null && data.Count > 0) return data;

          //  await AddDummyData();
            return await db.Table<Model.Contact>().OrderBy(o => o.Name).ToListAsync();
        }

        public async Task<Model.Contact> GetContact(int id)
        {
            var data = await db.Table<Model.Contact>().FirstOrDefaultAsync(f => f.Id == id);
            if (data != null) return data;

            return null;
        }

        /// <summary>
        ///  Insert new Contact/ Update already existing Contact 
        /// </summary>
        /// <returns></returns>
        public async Task<(string, bool)> Save(Model.Contact model)
        {
            try
            {
                var validationMessage = await this.EntryValidation(model);
                if (!string.IsNullOrEmpty(validationMessage))
                    return (validationMessage, false);

                if (model.Id == 0)
                    await db.InsertAsync(model);
                else
                    await db.UpdateAsync(model);

                return ("", true);
            }
            catch (Exception e)
            {
                return (e.Message, false);
            }

        }
        /// <summary>
        /// Delete Contact
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<(string, bool)> DeleteContact(int id)
        {
            try
            {
                if (id == 0)
                    return ("Parameeter List cannot be empty", false);

                var r = await db.DeleteAsync<Model.Contact>(id);

                return ("", true);
            }
            catch (Exception e)
            {
                return (e.Message, false);
            }
        }

        private async Task<string> EntryValidation(Model.Contact model)
        {
            if (string.IsNullOrEmpty(model.Name)) return "Please specify the Contact Name";
            else if (string.IsNullOrEmpty(model.PhoneNumber)) return "Please specify the Phone Number";

            var alreadyExist = await db.Table<Model.Contact>().FirstOrDefaultAsync(a => a.Id != model.Id &&
              (
                  a.Name.ToLower() == model.Name.ToLower()

              ));
            if (alreadyExist != null && alreadyExist.Id > 0)
                return "Contact Name already exists";

            alreadyExist = await db.Table<Model.Contact>().FirstOrDefaultAsync(a => a.Id != model.Id &&
           (
               a.PhoneNumber == model.PhoneNumber

           ));
            if (alreadyExist != null && alreadyExist.Id > 0)
                return "Contact Number already exists";

            if (model.Email != null)
            {
                alreadyExist = await db.Table<Model.Contact>().FirstOrDefaultAsync(a => a.Id != model.Id &&
                  (
                     a.Email != null && a.Email.ToLower() == model.Email.ToLower()

                  ));
                if (alreadyExist != null && alreadyExist.Id > 0)
                    return "Email already exists";
            }

            if (alreadyExist != null && alreadyExist.Id > 0)
                return "Contact already exists";

            return null;

        }



        public async Task AddDummyData()
        {
            var list = new List<Model.Contact>
            {
                new Model.Contact { Email="janaka@gmail.com", Name="Janaka Aravinda",   PhoneNumber ="0710441639", ProfilePic="logo.png"  },
                new Model.Contact { Email=" Anne@gmail.com", Name="Anne Perera",    PhoneNumber ="0710332395", ProfilePic="logo.png"  },
                new Model.Contact { Email=" Erik@gmail.com", Name="Jan Erik Evenger",    PhoneNumber ="0714344395", ProfilePic="logo.png"  },
                new Model.Contact { Email="Oliver@gmail.com", Name="Oliver Khan",   PhoneNumber ="0713434639", ProfilePic="logo.png"  },
                new Model.Contact { Email="Berry@gmail.com", Name="Berry Allen",    PhoneNumber ="0713434239", ProfilePic="logo.png"  },
                new Model.Contact { Email="Natalia@gmail.com", Name="Natalia Girn",  PhoneNumber ="0712222639", ProfilePic="logo.png"  },
                new Model.Contact { Email="Steve@gmail.com", Name="Devinda Perera",    PhoneNumber ="0714114163", ProfilePic="logo.png"  },
            };          

            await database.InsertAllAsync(list);

        }
    }
}
