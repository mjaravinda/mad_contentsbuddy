﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContentsBuddy.Model
{
    public class Contact
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string ProfilePic { get; set; }
        [Ignore]
        public bool IsSelected { get; set; } = false;
        [Ignore]
        public bool IsView { get; set; } = false;
    }
}
