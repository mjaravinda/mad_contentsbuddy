﻿using ContentsBuddy.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContentsBuddy
{
    public class MessageService: IMessageService
    {
        public async Task<bool> QuestionYesNoAsync(string message)
        {
            var res = await App.Current.MainPage.DisplayAlert("Question?", message, "Yes", "No");
            return res;
        }

        public async Task ShowAsync(string message)
        {
            await App.Current.MainPage.DisplayAlert("Contact Buddy", message, "Ok");
        }
    }
}
