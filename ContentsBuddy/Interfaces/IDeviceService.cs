﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContentsBuddy.Interfaces
{
    public interface IDeviceService
    {
        void MakeACall(string phoneNo);
        void SendAMessage(string phoneNo);
        void SendAEmail(string email);
    }
}
