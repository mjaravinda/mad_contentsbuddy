﻿using ContentsBuddy.Interfaces;
using ContentsBuddy.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ContentsBuddy.ViewModels
{
    public class ContactVM: BaseVM
    {
        public ICommand SaveCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand EntryOpenCommand { get; set; }
        public ICommand MakeAPhoneCallCommand { get; set; }
        public ICommand SendAMessageCommand { get; set; }
        public ICommand SendAEmailCommand { get; set; }
        public ICommand SearchCommand { get; set; }
        public ICommand SearchVisibleCommand { get; set; }


        INavigationService navigation;
        IDeviceService deviceAction;
        IMessageService messageService;
        ContactRepository repo;
        public ContactVM(INavigationService navigationService, IDeviceService deviceAction, IMessageService messageService)
        {
            repo = new ContactRepository();
            SearchVisible = false;
            this.navigation = navigationService;
            this.deviceAction = deviceAction;
            this.SaveCommand = new Command(SaveData);
            this.DeleteCommand = new Command(DeleteData);
            this.EntryOpenCommand = new Command(OpenEntryPage);
            this.MakeAPhoneCallCommand = new Command(MakeAPhoneCall);
            this.SendAMessageCommand = new Command(SendAMessage);
            this.SendAEmailCommand = new Command(SendAEmail);
            this.SearchCommand = new Command(SearchData);
            this.SearchVisibleCommand = new Command(SearchVisibleChanged);
            this.messageService = messageService;


        }
        public ContactVM()
        {
            repo = new ContactRepository();
            SearchVisible = false;
            this.SaveCommand = new Command(SaveData);
            this.DeleteCommand = new Command(DeleteData);
            this.EntryOpenCommand = new Command(OpenEntryPage);
            this.MakeAPhoneCallCommand = new Command(MakeAPhoneCall);
            this.SendAMessageCommand = new Command(SendAMessage);
            this.SendAEmailCommand = new Command(SendAEmail);
            this.SearchCommand = new Command(SearchData);
            this.SearchVisibleCommand = new Command(SearchVisibleChanged);
        }






        #region Properties

        private bool searchVisible;

        public bool SearchVisible
        {
            get { return searchVisible; }
            set
            {
                searchVisible = value; OnPropertyChanged();


            }
        }

        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                OnPropertyChanged();


            }
        }

        private Model.Contact contact;
        public Model.Contact Contact
        {
            get { return contact; }
            set
            {
                contact = value;
                base.OnPropertyChanged();
                base.OnPropertyChanged();
            }
        }
        public List<Model.Contact> AllContacts;
        private ObservableCollection<Model.Contact> contacts;
        public ObservableCollection<Model.Contact> Contacts
        {
            get { return contacts; }
            set
            {
                contacts = value;
                OnPropertyChanged();
            }
        }

        private string entryPageTitle;

        public string EntryPageTitle
        {
            get { return entryPageTitle; }
            set { entryPageTitle = value; }
        }

        private string sourceImage;

        public string SourceImage
        {
            get { return sourceImage; }
            set { sourceImage = value; OnPropertyChanged(); }
        }


        #endregion


        #region Methods
        public async void SaveData(object obj)
        {
            try
            {
                this.IsBusy = true;
                // this.Contact.ProfilePic = this.SourceImage;
                var result = await repo.Save(this.Contact);

                if (result.Item2)
                {
                    this.AllContacts = await repo.GetAllContacts();
                    this.Contacts = new ObservableCollection<Model.Contact>(this.AllContacts);
                    this.IsBusy = false;
                    await this.navigation.PopAsync();
                    //navigate back
                }
                else
                {
                    await messageService.ShowAsync(result.Item1);
                    this.IsBusy = false;
                    //validatiom message box
                }

            }
            catch (Exception e)
            {

                throw;
            }
        }

        private async void DeleteData(object obj)
        {
            var res = await messageService.QuestionYesNoAsync("Do you want to delete this contact ?");
            if (res)
            {
                this.IsBusy = true;
                var result = await repo.DeleteContact(this.Contact.Id);
                this.IsBusy = false;
                if (result.Item2)
                {
                    Contacts.Remove(Contacts.FirstOrDefault(f => f.Id == Contact.Id));
                    AllContacts.Remove(AllContacts.FirstOrDefault(f => f.Id == Contact.Id));
                    this.Contact = null;

                }
                else
                {
                    this.IsBusy = false;
                }

            }



        }

        public async Task GetAllContacts()
        {

            this.AllContacts = await repo.GetAllContacts();
            this.Contacts = new ObservableCollection<Model.Contact>(this.AllContacts);

        }

        public async Task GetContact(int id)
        {

            this.Contact = await repo.GetContact(id);


        }

        private async void OpenEntryPage(object obj)
        {
            var b = int.TryParse(obj?.ToString(), out var id);
            if (!b || id ==0) Contact = new Model.Contact { Id = 0 };
            await navigation.NavigateToAsync("ContactEntryPage", id);
        }

        private void SendAEmail(object obj)
        {
            if (obj != null)
                deviceAction.SendAEmail(obj.ToString());
        }

        private void SendAMessage(object obj)
        {
            if (obj != null)
                deviceAction.SendAMessage(obj.ToString());
        }

        private void MakeAPhoneCall(object obj)
        {
            if (obj != null)
                deviceAction.MakeACall(obj.ToString());
        }

        private void SearchData(object obj)
        {
            this.SearchItems();
        }



        public void SearchItems()
        {
            if (this.AllContacts != null && this.AllContacts.Count > 0)
            {
                if (String.IsNullOrEmpty(SearchText))
                {
                    this.Contacts = new ObservableCollection<Model.Contact>(this.AllContacts);

                }
                else
                {
                    var term = this.SearchText.ToLower();
                    var searchItems = this.AllContacts.Where(w =>
                        (w.Name != null && w.Name.ToLower().Contains(term))
                        || (w.PhoneNumber != null && w.Name.ToLower().Contains(term))
                        || (w.Email != null && w.Name.ToLower().Contains(term))

                    ).ToList();
                    Contacts = new ObservableCollection<Model.Contact>(searchItems);
                }
            }


        }

        private void SearchVisibleChanged(object obj)
        {
            this.SearchVisible = !this.SearchVisible;
            this.SearchText = null;
            this.Contacts = new ObservableCollection<Model.Contact>(this.AllContacts);


        }

     



        #endregion
    }
}
