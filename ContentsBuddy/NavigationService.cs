﻿using ContentsBuddy.Interfaces;
using ContentsBuddy.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContentsBuddy
{
    public class NavigationService : INavigationService
    {
       
    private ContentPage page;
    public NavigationService(ContentPage page)
    {
        this.page = page;
    }

    public async Task NavigateToAsync(string route, int id)
    {
        if (route == "ContactEntryPage")
        {         
           await page.Navigation.PushAsync(new ContactEntryPage(id));
        }

    }

    public async Task PopAsync()
    {
        await page.Navigation.PopToRootAsync();

    }
}
}
