﻿using ContentsBuddy.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContentsBuddy
{
    public class DeviceService: IDeviceService
    {
        public void MakeACall(string phoneNo)
        {
            if (PhoneDialer.Default.IsSupported)
                PhoneDialer.Default.Open(phoneNo);
        }

        public async void SendAEmail(string email)
        {
            if (Email.Default.IsComposeSupported)
            {
                string[] recipients = new[] { email };

                var message = new EmailMessage
                {
                    Subject = "",
                    Body = "",
                    BodyFormat = EmailBodyFormat.PlainText,
                    To = new List<string>(recipients)
                };

                await Email.Default.ComposeAsync(message);
            }
        }

        public async void SendAMessage(string phoneNo)
        {
            if (Sms.Default.IsComposeSupported)
            {
                string[] numbers = new[] { phoneNo };
                var message = new SmsMessage("", numbers);
                await Sms.Default.ComposeAsync(message);
            }
        }
    }
}
