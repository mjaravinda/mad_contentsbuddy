using ContentsBuddy.Interfaces;
using ContentsBuddy.ViewModels;

namespace ContentsBuddy.Views;

public partial class ContactEntryPage : ContentPage
{
    ContactVM vm;
    int currentId;
    private bool isInitial = true;
    public ContactEntryPage(int id)
    {
        vm = new ContactVM(new NavigationService(this), new DeviceService(), new MessageService());
        InitializeComponent();
        this.BindingContext = this.vm;
        vm.Contact = new Model.Contact();
        this.currentId = id;
    }



    private void nameEntry_Completed(object sender, EventArgs e)
    {
        contactNumberEntry.Focus();
    }

    private void contactNumberEntry_Completed(object sender, EventArgs e)
    {
        emailEntry.Focus();
    }

    private void emailEntry_Completed(object sender, EventArgs e)
    {
        vm.SaveData(null);
        //	updateButtton.Focus(); 
    }
    protected override async void OnAppearing()
    {
        if (isInitial)
        {
            if (currentId == 0) vm.Contact = new Model.Contact { ProfilePic = "logo.svg" };
            else if (currentId > 0)
            {
                vm.IsBusy = true;
                await vm.GetContact(currentId);


                vm.IsBusy = false;
            }
            profileImage.Source = vm.Contact.ProfilePic;
        }

    }



    private async void OpenByGallary(object sender, EventArgs e)
    {
        try
        {
            isInitial = false;
            if (MediaPicker.Default.IsCaptureSupported)
            {
                FileResult photo = await MediaPicker.Default.PickPhotoAsync();

                if (photo != null)
                {
                    vm.Contact.ProfilePic = await SaveOnLocalAsync(photo);
                    profileImage.Source = vm.Contact.ProfilePic;

                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    private async void CaptureAPhoto(object sender, EventArgs e)
    {
        isInitial = false;
        if (MediaPicker.Default.IsCaptureSupported)
        {
            FileResult photo = await MediaPicker.Default.CapturePhotoAsync();

            if (photo != null)
            {
                vm.Contact.ProfilePic = await SaveOnLocalAsync(photo);
                if (vm.Contact.ProfilePic == null)
                    profileImage.Source = "logo.svg";
                else
                    profileImage.Source = vm.Contact.ProfilePic;

            }
        }
    }


    async Task<string> SaveOnLocalAsync(FileResult photo)
    {

        if (photo == null)
        {
            return null;
        }
        var newFile = Path.Combine(FileSystem.CacheDirectory, photo.FileName);
        using (Stream stream = await photo.OpenReadAsync())
        using (var newStream = File.OpenWrite(newFile))
        {
            await stream.CopyToAsync(newStream);
        }
        return newFile;
    }
}