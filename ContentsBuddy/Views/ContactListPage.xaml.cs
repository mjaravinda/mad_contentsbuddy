using ContentsBuddy.ViewModels;

namespace ContentsBuddy.Views;

public partial class ContactListPage : ContentPage
{
	
	ContactVM vm;
    public ContactListPage()
    {
        InitializeComponent();
        vm = new ContactVM(new NavigationService(this), new DeviceService(), new MessageService());
        this.BindingContext = vm;
    }



    protected async override void OnAppearing()
    {
        vm.IsBusy = true;
        await vm.GetAllContacts();
        vm.IsBusy = false;
    }
}